<?php

namespace Ct\Debug;

error_reporting(-1);

/**
 * Class Debug
 *
 * @package Ct\Debug
 * @author Artem Chernovol artem.chernovol@gmail.com
 */
class Debug
{
    /**
     * @param $content
     * @param null $name
     */
    public static function varExportAndDie($content, $name = null)
    {
        if ($content) {
            try {
                $serialized = serialize($content);
                $content = unserialize($serialized);
            } catch (\Exception $ex) {
                $content = 'Closure in class ' . get_class($content);
            }
        }

        self::makeOutput(
            $content,
            $name
        );

        die();
    }

    /**
     * @param $content
     * @param null $name
     */
    public static function varExport($content, $name = null)
    {
        if ($content) {
            $serialized = serialize($content);
            $content = unserialize($serialized);
        }

        self::makeOutput(
            $content,
            $name
        );
    }

    /**
     * @param $content
     * @param null $name
     */
    public static function varExportAndDiePlain($content, $name = null)
    {
        if ($content) {
            try {
                $serialized = serialize($content);
                $content = unserialize($serialized);
            } catch (\Exception $ex) {
                $content = 'Closure in class ' . get_class($content);
            }
        }

        self::makeOutput(
            $content,
            $name,
            true
        );

        die();
    }

    /**
     * @param $content
     * @param null $name
     */
    public static function varExportPlain($content, $name = null)
    {
        if ($content) {
            $serialized = serialize($content);
            $content = unserialize($serialized);
        }

        self::makeOutput(
            $content,
            $name,
            true
        );
    }

    /**
     * @param $content
     * @param null $name
     */
    public static function varDumpAndDie($content, $name = null)
    {
        if ($content) {
            $serialized = serialize($content);
            $content = unserialize($serialized);
        }

        ob_start();
        var_dump($content);
        $content = ob_get_clean();

        self::makeOutput(
            $content,
            $name
        );

        die();
    }

    /**
     * @param $content
     * @param null $name
     */
    public static function varDump($content, $name = null)
    {
        if ($content) {
            $serialized = serialize($content);
            $content = unserialize($serialized);
        }

        ob_start();
        var_dump($content);
        $content = ob_get_clean();

        self::makeOutput(
            $content,
            $name
        );
    }

    /**
     * @param $isConsole
     *
     * @return string
     */
    public static function makeInfo($isConsole)
    {
        $html = '';

        try {
            throw new \Exception('Fake!');
        } catch (\Exception $ex) {
            $trace = $ex->getTrace();
            $filePath = isset($trace[2]['file']) ? $trace[2]['file'] : 'unknown';
            $line = isset($trace[2]['line']) ? $trace[2]['line'] : 'unknown';
            $class = isset($trace[3]['class']) ? $trace[3]['class'] : 'unknown';
            $function = isset($trace[3]['function']) ? $trace[3]['function'] : 'unknown';

            if (!$isConsole) {
                if (defined('APPLICATION_ROOT')) {
                    $filePath = str_replace(APPLICATION_ROOT, '', $filePath);
                }

                $part = explode(DIRECTORY_SEPARATOR, $filePath);
                $fileName = array_pop($part);
                $filePath = implode(DIRECTORY_SEPARATOR, $part);

                $fullTraceId = uniqid('debug-fullTrace');
                $element = 'document.getElementById(\'' . $fullTraceId . '\')';

                $html .= '<div class="ct-debug__info"><div>'
                    . '<span class="ct-debug__file-path">' . $filePath . DIRECTORY_SEPARATOR
                    . '<span class="ct-debug__file-name">' . $fileName . '</span></span>: '
                    . '<span class="ct-debug__line">' . $line . '</span>, '
                    . '<span class="ct-debug__class">' . $class . '</span>, '
                    . '<span class="ct-debug__function">' . $function . '</span> '
                    . '<a href="javascript:void(0)" onclick="' . $element . '.style.display = ('
                    . $element
                    . '.style.display == \'none\') ? \'block\' : \'none\'; return false;">Trace</a>'
                    . '</div>';

                $trace = '';
                $i = 0;
                foreach ($ex->getTrace() as $traceItem) {
                    $args = [];
                    $args = implode(',', $args);

                    $type = isset($traceItem['type']) ? $traceItem['type'] : '';
                    $function = isset($traceItem['function']) ? $traceItem['function'] : '';
                    $class = isset($traceItem['class']) ? $traceItem['class'] : '';
                    $line = isset($traceItem['line']) ? $traceItem['line'] : '';
                    $filePath = isset($traceItem['file']) ? $traceItem['file'] : '';

                    if ($class == __CLASS__) {
                        continue;
                    }

                    if (defined('APPLICATION_ROOT')) {
                        $filePath = str_replace(APPLICATION_ROOT, '', $filePath);
                    }

                    $trace .= sprintf('%3d', $i);

                    if ($filePath && $line) {
                        $trace .= " {$filePath}:{$line}";
                    }
                    $trace .= " {$class}{$type}{$function}($args)\n";

                    $i++;
                }
                // HERE: display:none; without space after : - this is important, ct-page replaces "display: none";
                $html .= '<div id="' . $fullTraceId . '" class="ct-debug__trace" style="display:none;">' . $trace
                    . '</div>';
                $html .= '</div>';
            } else {
                if (defined('APPLICATION_ROOT')) {
                    $filePath = str_replace(APPLICATION_ROOT, '', $filePath);
                }
                $html .= "$filePath:$line, $class, $function";
            }

            return $html;
        }
    }

    /**
     * @param $content
     * @param null $name
     * @param bool $shouldForcePlain
     */
    public static function makeOutput($content, $name = null, $shouldForcePlain = false)
    {
        // Add [textual] view of timestamp
        if (is_scalar($content) && preg_match('~^[\d]{9,}$~', $content)) {
            $content .= ' [' . date('Y-m-d H:i:s', $content) . ']';
        }

        $content = highlight_string(var_export($content, true), true);

        // HERE: native match() returns TRUE for "*/*" (it always is placed in the end of header string)
        $isJson = !empty($_SERVER['HTTP_ACCEPT']) && 0 === strpos($_SERVER['HTTP_ACCEPT'], 'application/json');

        if (isset($_SERVER['HTTP_HOST']) && !$isJson && !$shouldForcePlain) {
            $html = '<div class="ct-debug">';

            $html .= self::makeInfo(false);
            if ($name) {
                $html .= '<div class="ct-debug__name">' . $name . '</div>';
            }
            $html .= '<div class="DEBUG_CONTENT">' . $content . '</div>';
            $html .= '</div>';
            $html .= self::$css;

            echo $html;
        } elseif ($isJson) {
            echo "\n";
            echo self::makeInfo(true);
            if ($name) {
                echo $name;
            }
            echo str_replace('&gt;', '>', str_replace('&nbsp;', ' ', strip_tags($content)));
        } else {
            echo "\n";
            echo self::makeInfo(true);
            if ($name) {
                echo $name;
            }
            echo str_replace('&gt;', '>', str_replace('&nbsp;', ' ', strip_tags($content)));
            echo "\n";
        }
    }

    protected static $css = '
<style type="text/css">
	.ct-debug {
		position: relative;
		z-index: 10000;
		padding: 10px;
		margin: 10px;
		border: 2px solid #B71C1C;
		background-color: #FFEBEE;

		font-family: Consolas !important;
		font-size: 13px;
		line-height: 120%;
		color: #000000;
	}

	.ct-debug * {
		font-family: Consolas !important;
		font-size: 13px !important;
		line-height: 120% !important;
	}

	.ct-debug a {
		color: #0D47A1 !important;
		text-decoration: underline;
	}

	.ct-debug__info {
		margin-bottom: 10px;
	}

	.ct-debug__trace {
		margin-top: 10px;
		font-size: 12px;
		line-height: 120% !important;
		min-width: 2000px;
		white-space: pre-wrap;
	}

	.ct-debug__file-path {
		color: #7986CB;
	}

	.ct-debug__file-name {
		color: #1A237E;
		font-weight: bold;
	}

	.ct-debug__line {
		color: #ffffff;
		background-color: #1A237E;
		padding: 0 5px;
	}

	.ct-debug__class {
		color: #004D40;
	}

	.ct-debug__function {
		color: #BF360C;
	}

	.ct-debug__name {
		font-weight: bold;
		margin-bottom: 10px;
    }
</style>
	';
}
