<?php
define('APPLICATION_ROOT', realpath(dirname(__DIR__)));
define('DOCUMENT_ROOT', realpath(dirname(__DIR__ . '/public')));

define('STATIC_HEIGHT', 100);
define('STATIC_WIDTH', 100);
define('PER_PAGE', 3);

define('FILE_PATH', DOCUMENT_ROOT . '/files/image/');

define('ENVIRONMENT', 'local');
//define('ENVIRONMENT', 'development');

require_once APPLICATION_ROOT . '/../Debug.php';
require_once DOCUMENT_ROOT . '/core/Application.php';

Application::processRoute();
session_start();
?>
<!DOCTYPE html >
<html lang="en">
    <head>
        <title>Tasker</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="/styles/reset.css">
        <link rel="stylesheet" href="/styles/bootstrap.css">
        <link rel="stylesheet" href="/styles/bootstrap-theme.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"
            type="text/css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <script type="text/javascript" src="scripts/front.js"></script>
    </head>
    <body>
        <div class="container">
            <header class="page-header">
                <?php require_once DOCUMENT_ROOT . '/controller/partial/menu.php' ?>
            </header>
            <?php Application::runController() ?>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="text-muted">chernovol.taras@gmail.com</div>
            </div>
        </footer>

        <?php if (!empty($_SESSION['messageList'])): ?>
            <div class="messageWrapper">
                <?php foreach ($_SESSION['messageList'] as $message): ?>
                    <?php echo $message ?>
                <?php endforeach ?>
            </div>
        <?php endif ?>
        <?php unset($_SESSION['messageList']) ?>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="scripts/bootstrap.js"></script>
    </body>
</html>