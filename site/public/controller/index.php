<?php

require DOCUMENT_ROOT . '/core/Storage.php';

$storage = Storage::getInstance();

$currentPage = empty($_GET['page']) ? 1 : (int)$_GET['page'];
$currentPage = $currentPage < 1 ? 1 : $currentPage;

$result = $storage->select('task', $currentPage);
?>
    <div class="row">
        <?php if (!empty($result['rowList'])): ?>
            <?php foreach ($result['rowList'] as $task): ?>
                <?php if ($task['imageWidth'] > $task['imageHeight']): ?>
                    <?php $height = (STATIC_WIDTH / ($task['imageWidth'] / $task['imageHeight'])) ?>
                    <?php $marginTop = (STATIC_WIDTH - $height) / 2 ?>
                    <div class="col-sm-6 col-md-4"  >
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="image">
                                    <?php if (!empty($_SESSION['auth'])): ?>
                                        <a title="Delete" class="task-delete" href="/delete?taskId=<?php echo $task['taskId'] ?>"><span
                                                class="fa fa-times" aria-hidden="true"></span></a>
                                        <a title="Edit" class="task-edit" href="/edit?taskId=<?php echo $task['taskId'] ?>"><span
                                                class="fa fa-pencil" aria-hidden="true"></span></a>
                                    <?php endif ?>
                                    <img style="margin-top: <?php echo (int)($marginTop) ?>px;"
                                        width="<?php echo STATIC_WIDTH ?>" height="<?php echo (int)($height) ?>"
                                        src="<?php echo htmlspecialchars($task['image']) ?>">
                                </div>
                                <h3><?php echo htmlspecialchars($task['name']) ?></h3>
                                <?php if (!empty($task['status'])): ?>
                                    <div class="complete-item"><span title="Completed" class="fa fa-check"></span></div>
                                <?php endif ?>
                                <h4><?php echo htmlspecialchars($task['email']) ?></h4>
                                <p><?php echo htmlspecialchars($task['text']) ?></p>
                            </div>
                        </div>
                    </div>
                <?php elseif ($task['imageWidth'] < $task['imageHeight']): ?>
                    <?php $width = (STATIC_HEIGHT / ($task['imageHeight'] / $task['imageWidth'])) ?>
                    <?php $marginLeft = (STATIC_HEIGHT - $width) / 2 ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="image">
                                    <?php if (!empty($_SESSION['auth'])): ?>
                                        <a title="Delete" class="task task-delete" href="/delete?taskId=<?php echo $task['taskId'] ?>"><span
                                                class="fa fa-times" aria-hidden="true"></span></a>
                                        <a title="Edit" class="task task-edit" href="/edit?taskId=<?php echo $task['taskId'] ?>"><span
                                                class="fa fa-pencil" aria-hidden="true"></span></a>
                                    <?php endif ?>

                                    <img style="margin-left: <?php echo (int)($marginLeft) ?>px;"
                                        width="<?php echo (int)($width) ?>" height="<?php echo STATIC_HEIGHT ?>"
                                        src="<?php echo htmlspecialchars($task['image']) ?>">
                                </div>
                                <h3><?php echo htmlspecialchars($task['name']) ?></h3>
                                <?php if (!empty($task['status'])): ?>
                                    <div class="complete-item"><span title="Completed" class="fa fa-check"></span></div>
                                <?php endif ?>
                                <h4><?php echo htmlspecialchars($task['email']) ?>
                                </h4>
                                <p><?php echo htmlspecialchars($task['text']) ?></p></div>
                        </div>
                    </div>
                <?php elseif ($task['imageWidth'] == $task['imageHeight']): ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption">
                                <div class="image">
                                    <?php if (!empty($_SESSION['auth'])): ?>
                                        <a title="Delete" class="task task-delete" href="/delete?taskId=<?php echo $task['taskId'] ?>"><span
                                                class="fa fa-times" aria-hidden="true"></span></a>
                                        <a title="Edit" class="task task-edit" href="/edit?taskId=<?php echo $task['taskId'] ?>"><span
                                                class="fa fa-pencil" aria-hidden="true"></span></a>
                                    <?php endif ?>
                                    <img height="<?php echo STATIC_WIDTH ?>" width="<?php echo STATIC_HEIGHT ?>"
                                        src="<?php echo htmlspecialchars($task['image']) ?>">
                                </div>
                                <h3><?php echo htmlspecialchars($task['name']) ?></h3>
                                <?php if (!empty($task['status'])): ?>
                                    <div class="complete-item"><span title="Completed" class="fa fa-check"></span></div>
                                <?php endif ?>
                                <h4><?php echo htmlspecialchars($task['email']) ?>
                                </h4>
                                <p><?php echo htmlspecialchars($task['text']) ?></p></div>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif ?>
    </div>

<?php if (count($result['pageList']) > 1): ?>
    <nav aria-label="...">
        <ul class="pagination">
            <li><a href="/?page=1"><span class="fa fa-angle-double-left"></span></a></li>
            <?php if ($currentPage > 1): ?>
                <li><a href="/?page=<?php echo (int)($currentPage - 1) ?>"><span class="fa fa-angle-left"></span></a>
                </li>
            <?php else: ?>
                <li><a href="/?page=<?php echo (int)($currentPage) ?>"><span class="fa fa-angle-left"></span></a></li>
            <?php endif ?>

            <?php foreach ($result['pageList'] as $page): ?>
                <?php if ($page == $result['currentPage']): ?>
                    <li class="active"><a><?php echo $page ?></a></li>
                <?php else: ?>
                    <li><a href="/?page=<?php echo $page ?>"><?php echo $page ?></a></li>
                <?php endif ?>
            <?php endforeach ?>

            <?php if ($currentPage < $result['pageCount']): ?>
                <li><a href="/?page=<?php echo (int)($currentPage + 1) ?>"><span class="fa fa-angle-right"></span></a>
                </li>
            <?php else: ?>
                <li><a href="/?page=<?php echo (int)($currentPage) ?>"><span class="fa fa-angle-right"></span></a></li>
            <?php endif ?>
            <li><a href="/?page=<?php echo (int)($result['pageCount']) ?>"><span
                        class="fa fa-angle-double-right"></span></a></li>
        </ul>
    </nav>
<?php endif ?>