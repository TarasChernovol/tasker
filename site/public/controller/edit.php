<?php

require DOCUMENT_ROOT . '/core/Storage.php';

$storage = Storage::getInstance();

$id = empty($_GET['taskId']) ? null : (int)$_GET['taskId'];
$text = empty($_POST['text']) ? null : htmlspecialchars($_POST['text']);
$status = empty($_POST['complete']) ? null : htmlspecialchars($_POST['complete']);

if ($text == null) {
    $task = $storage->selectOne('task', $id);
} else {
    $storage->update('task',
        $id,
        [
            'text' => $text,
            'status' => $status,
        ]);
    $task = $storage->selectOne('task', $id);
    $_SESSION['messageList'] = ['Task was updated',];
}
?>
<section style="min-width: 290px; max-width: 500px">
    <form role="form" action="/edit?taskId=<?php echo (int)$id ?>" method="post" enctype="multipart/form-data">
        <h4 class="edit-header">EDIT <span><?php echo htmlspecialchars($task['name']) ?></span></h4>
        <div class="form-group">
            <label for="textInput">Text</label>
            <textarea name="text" class="form-control" id="textInput"
                rows="6"><?php echo htmlspecialchars($task['text']) ?></textarea>
        </div>
        <div class="checkbox">
            <label><input name="complete" type="checkbox" value="Complete">Is completed?</label>
        </div>
        <button type="submit" class="btn btn-success">UPDATE</button>
    </form>
</section>