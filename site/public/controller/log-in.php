<?php

require_once DOCUMENT_ROOT . '/core/Storage.php';

$storage = Storage::getInstance();

$email = empty($_POST['email']) ? null : htmlspecialchars($_POST['email']);
$password = empty($_POST['password']) ? null : htmlspecialchars($_POST['password']);

$user = $storage->getEmailAndPassword('user');

if ($email == $user['email'] and $password == $user['password']) {
    $_SESSION['auth'] = [$user['email']];
    header('Location: /');
    exit;
} else {
    if ($email and $password !== null) {
        $_SESSION['messageList'] = ['Invalid credentials',];
    }
}
?>
<section style="min-width: 290px; max-width: 500px">
    <form class="form-horizontal" role="form" action="/log-in" method="post">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input name="password" type="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Sign in</button>
            </div>
        </div>
    </form>
</section>