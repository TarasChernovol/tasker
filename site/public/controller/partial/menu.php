<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Tasker</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php if (Application::$route == '/'): ?>
                    <li class="active"><a href="/">HOME</a></li>
                    <li><a href="/add">ADD TASK</a></li>
                    <li><a href="/log-in">LOG IN</a></li>
                <?php elseif (Application::$route == '/add'): ?>
                    <li><a href="/">HOME</a></li>
                    <li class="active"><a href="/add">ADD TASK</a></li>
                    <li><a href="/log-in">LOG IN</a></li>
                <?php elseif (Application::$route == '/log-in'): ?>
                    <li><a href="/">HOME</a></li>
                    <li><a href="/add">ADD TASK</a></li>
                    <li class="active"><a href="/log-in">LOG IN</a></li>
                <?php elseif (Application::$route == '/edit'): ?>
                    <li><a href="/">HOME</a></li>
                    <li><a href="/add">ADD TASK</a></li>
                    <li><a href="/log-in">LOG IN</a></li>
                <?php endif ?>
            </ul>
            <?php if (!empty($_SESSION['auth'])): ?>
                <?php foreach ($_SESSION['auth'] as $email): ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="mailto:<?php echo htmlspecialchars($email) ?>"><?php echo $email ?></a></li>
                        <li><a href="/log-out">LOG OUT</a></li>
                    </ul>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</nav>