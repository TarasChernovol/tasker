<?php

require_once DOCUMENT_ROOT . '/core/Storage.php';

$storage = Storage::getInstance();

$id = (int)$_GET['taskId'];
$storage->delete('task', $id);
$_SESSION['messageList'] = ['Task was deleted',];
header('Location: /');
exit;