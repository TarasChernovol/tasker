<?php

require DOCUMENT_ROOT . '/core/Storage.php';

$storage = Storage::getInstance();

$name = empty($_POST['name']) ? null : htmlspecialchars($_POST['name']);
$email = empty($_POST['email']) ? null : htmlspecialchars($_POST['email']);
$text = empty($_POST['text']) ? null : htmlspecialchars($_POST['text']);
$image = empty($_FILES['file']['tmp_name']) ? null : $_FILES['file']['tmp_name'];
$imageName = empty($_FILES['file']['name']) ? null : htmlspecialchars($_FILES['file']['name']);

if ($name and $email and $text and $image and $imageName !== null) {
    move_uploaded_file($image, DOCUMENT_ROOT . '/tmp/' . $imageName);
    $imageWidth = (int)getimagesize(DOCUMENT_ROOT . '/tmp/' . $imageName)[0];
    $imageHeight = (int)getimagesize(DOCUMENT_ROOT . '/tmp/' . $imageName)[1];
    $imageId = $storage->insert('task',
        [
            'name' => $name,
            'email' => $email,
            'text' => $text,
            'imageWidth' => $imageWidth,
            'imageHeight' => $imageHeight,
        ]);

    $directoryPath = FILE_PATH . $imageId;
    if (!file_exists($directoryPath)) {
        mkdir($directoryPath, 0777);
    }

    imagejpeg(imagecreatefromjpeg(DOCUMENT_ROOT . '/tmp/' . $imageName), $directoryPath . '/' . $imageName, 100);

    $imageSrc = '/files/image/' . $imageId . '/' . $imageName;
    $storage->update('task', $imageId, ['image' => $imageSrc,]);

    $_SESSION['messageList'] = ['Task was saved',];
}
?>
<form id="add-form" role="form" action="/add" method="post"
    enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control" id="name" placeholder="" value="">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input name="email" type="email" class="form-control" id="email" placeholder="" value="">
    </div>
    <div class="form-group">
        <label for="text">Text</label>
        <textarea name="text" class="form-control" id="text" rows="6"></textarea>
    </div>
    <div class="form-group">
        <label for="file">Image</label>
        <input name="file" type="file" id="file" onchange="previewImage(this);">
    </div>
    <button type="submit" class="btn btn-success">Save</button>
    <input type="button" name="btn" value="Preview" id="preview-button" data-toggle="modal" data-target="#confirm-submit" class="btn btn-default" />
</form>

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Confirm Submit
            </div>
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <th>Name</th>
                        <td id="namePreview"></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td id="emailPreview"></td>
                    </tr>
                    <tr>
                        <th>Text</th>
                        <td id="textPreview"></td>
                    </tr>
                    <tr>
                        <img style="width: 100%" id="image-preview" src="#" alt="" />
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="submit" type="submit" class="btn btn-success">Save</button>
            </div>
        </div>
    </div>
</div>