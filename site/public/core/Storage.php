<?php

require_once DOCUMENT_ROOT . '/core/Application.php';

/**
 * Class Storage
 */
class Storage
{
    /** @var Storage */
    private static $_instance = null;

    /**
     * @return Storage|null
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new Storage();
        }

        return self::$_instance;
    }

    /** @var \PDO */
    protected $_pdo = null;

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        $config = Application::getConfig();

        if ($this->_pdo === null) {
            $this->_pdo = new PDO($config['db']['dsn'], $config['db']['username'], $config['db']['password'], [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]);
        }

        return $this->_pdo;
    }

    /**
     * @param $tableName
     * @param $data
     *
     * @return string
     */
    public function insert($tableName, $data)
    {
        $fieldNameList = [];
        $placeholderNameList = [];
        foreach ($data as $key => $value) {
            $fieldNameList[] = '`' . $key . '`';
            $placeholderNameList[] = ':' . $key;
        }

        $query = 'INSERT INTO `' . $tableName . '`('
            . implode(',', $fieldNameList)
            . ') VALUES('
            . implode(',', $placeholderNameList) . ')';
        $statement = $this->getPdo()->prepare($query);

        foreach ($data as $key => $value) {
            $statement->bindValue(':' . $key, $value, PDO::PARAM_STR);
        }

        $statement->execute();

        return $this->getPdo()->lastInsertId();
    }

    /**
     * @param $tableName
     * @param $id
     * @param $data
     */
    public function update($tableName, $id, $data)
    {
        $valueList = [];
        foreach ($data as $key => $value) {
            $valueList[] = '`' . $key . '` = :' . $key;
        }

        $query = 'UPDATE `' . $tableName . '` SET ' . implode(',', $valueList) . ' WHERE `' . $tableName . 'Id` = :id';
        $statement = $this->getPdo()->prepare($query);

        foreach ($data as $key => $value) {
            $statement->bindValue(':' . $key, $value, PDO::PARAM_STR);
        }

        $statement->bindValue(':id', $id);

        $statement->execute();
    }

    /**
     * @param $tableName
     * @param $currentPage
     *
     * @return array|mixed
     */
    public function select($tableName, $currentPage)
    {
        $result = [
            'rowList' => [],
            'pageList' => [],
            'currentPage' => $currentPage,
        ];

        $query = 'SELECT SQL_CALC_FOUND_ROWS * FROM `' . $tableName . '` ORDER BY `' . $tableName
            . 'Id` DESC LIMIT :limit OFFSET :offset';

        $statement = $this->getPdo()->prepare($query);
        $statement->bindValue(':offset', ($currentPage - 1) * PER_PAGE, PDO::PARAM_INT);
        $statement->bindValue(':limit', (int)PER_PAGE, PDO::PARAM_INT);

        $statement->execute();

        $result['rowList'] = $statement->fetchAll(PDO::FETCH_NAMED);

        //	Pagination
        //

        $statement = $this->_pdo->prepare('SELECT FOUND_ROWS()');
        $statement->execute();

        $result_ = $statement->fetch();
        $totalRowCount = $result_[0];

        $sliderSize = 5;
        $pageCount = ceil($totalRowCount / PER_PAGE);

        if ($sliderSize > $pageCount) {
            $sliderSize = $pageCount;
        }

        $delta = ceil($sliderSize / 2);

        if ($currentPage - $delta > $pageCount - $sliderSize) {
            $lowerBound = $pageCount - $sliderSize + 1;
            $upperBound = $pageCount;
        } else {
            if ($currentPage - $delta < 0) {
                $delta = $currentPage;
            }

            $offset = $currentPage - $delta;
            $lowerBound = $offset + 1;
            $upperBound = $offset + $sliderSize;
        }

        $lowerBound = $this->_normalizePageNumber($lowerBound, $pageCount);
        $upperBound = $this->_normalizePageNumber($upperBound, $pageCount);

        $pageList = [];

        for ($currentPage = $lowerBound; $currentPage <= $upperBound; $currentPage++) {
            $pageList[$currentPage] = $currentPage;
        }

        $result['pageCount'] = $pageCount;
        $result['totalRowCount'] = $totalRowCount;
        $result['pageList'] = $pageList;

        return $result;
    }

    /**
     * @param $currentPage
     * @param $pageCount
     *
     * @return int
     */
    protected function _normalizePageNumber($currentPage, $pageCount)
    {
        $currentPage = (int)$currentPage;

        if ($currentPage < 1) {
            $currentPage = 1;
        }

        if ($pageCount > 0 && $currentPage > $pageCount) {
            $currentPage = $pageCount;
        }

        return $currentPage;
    }

    /**
     * @param $tableName
     *
     * @param $id
     */
    public function delete($tableName, $id)
    {
        $query = 'DELETE FROM `' . $tableName . '`  WHERE `' . $tableName . 'Id` = :id';

        $statement = $this->getPdo()->prepare($query);

        $statement->bindValue(':id', $id);

        $statement->execute();
    }

    /**
     * @param $tableName
     *
     * @param $id
     */
    public function selectOne($tableName, $id)
    {
        $query = 'SELECT * FROM `' . $tableName . '` WHERE `' . $tableName . 'Id` = :id';

        $statement = $this->getPdo()->prepare($query);

        $statement->bindValue(':id', $id);

        $statement->execute();

        $result = $statement->fetchAll(PDO::FETCH_NAMED);

        return $result[0];
    }

    /**
     * @param $tableName
     *
     * @return mixed
     */
    public function getEmailAndPassword($tableName)
    {
        $query = 'SELECT * FROM `' . $tableName . '`';

        $statement = $this->getPdo()->prepare($query);

        $statement->execute();

        $result = $statement->fetchAll(PDO::FETCH_NAMED);

        return $result[0];
    }
}