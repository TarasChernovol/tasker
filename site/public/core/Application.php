<?php

/**
 * Class Application
 */
class Application
{
    static public $route;

    /**
     *
     */
    static public function processRoute()
    {
        $part = explode('?', $_SERVER['REQUEST_URI']);
        self::$route = $part[0];
    }

    /**
     *
     */
    static public function runController()
    {
        switch (self::$route) {
            case('/'):
                require_once DOCUMENT_ROOT . '/controller/index.php';
                break;
            case('/add'):
                require_once DOCUMENT_ROOT . '/controller/add.php';
                break;
            case('/log-in'):
                require_once DOCUMENT_ROOT . '/controller/log-in.php';
                break;
            case('/edit'):
                require_once DOCUMENT_ROOT . '/controller/edit.php';
                break;
            case('/delete'):
                require_once DOCUMENT_ROOT . '/controller/delete.php';
                break;
            case('/log-out'):
                require_once DOCUMENT_ROOT . '/controller/log-out.php';
                break;
            default:
                echo 'Page was not found';
                break;
        }
    }

    /**
     * @return string
     */
    static public function getConfig()
    {
        return require_once APPLICATION_ROOT . '/config/env/' . ENVIRONMENT . '.php';
    }
}