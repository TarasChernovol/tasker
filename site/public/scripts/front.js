$(document).ready(function () {
    $('#preview-button').click(function () {
        $('#namePreview').text($('#name').val());
        $('#emailPreview').text($('#email').val());
        $('#textPreview').text($('#text').val());
    });

    $('#submit').click(function () {
        $('#add-form').submit();
    });
});

function previewImage(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

